import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class ReceiveMessage{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;

	private static String sugarUrl = "";
	private static String sugarUser = "";
	private static String sugarPass = "";	
	private static String pmseProcess = "";
	private static String titleCase = "";	
	private static String lastName = "";	
	private static String engineReceiveMessage = "";
	private static String engineEventBased = "";
	private static String engineAddRelated = "";

	public static void main(String[] args) throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		sugarUrl = TestEngine.getConfig("sugar.url");
		sugarUser = TestEngine.getConfig("sugar.name");
		sugarPass = TestEngine.getConfig("sugar.password");	
		engineReceiveMessage = TestEngine.getConfig("engine.receiveMessage");
		engineEventBased = TestEngine.getConfig("engine.eventBased");
		engineAddRelated = TestEngine.getConfig("engine.addRelated");
		
		TestEngine.testCaseClean(engineReceiveMessage);
		TestEngine.testCaseClean(engineEventBased);
		TestEngine.testCaseClean(engineAddRelated);
		
		System.out.println ("Login to SugarCRM...");
		engine.openBrowser(sugarUrl);		
		engine.loginSugar(sugarUser,sugarPass, true);
		System.out.println ("Creating a Lead...");		
		lastName = "Due" + TestEngine.getRandomString(6);
		titleCase = "John " + lastName;
		engine.createLead(lastName, "Receive Message");

		try {		
			System.out.println ("Verify Add Related Record...");	
			System.out.println ("-------------------------------");		
			engine.closeTask("Task " + lastName,titleCase);
			TestEngine.testCasePassed(engineAddRelated);
			engine.logoutSugar();		
			System.out.println ("Verify Event Base Gateway...");	
			System.out.println ("Verify Receive Message...");				
			engine.loginSugar(sugarUser,sugarPass, false);	
			engine.openCase(titleCase);				
			engine.derivateCase("Approve");			
			TestEngine.testCasePassed(engineEventBased);
			TestEngine.testCasePassed(engineReceiveMessage);			

			engine.logoutSugar();
		}catch (Exception e) {
			TestEngine.testCaseFailed(engineAddRelated);
			TestEngine.testCaseFailed(engineEventBased);
			TestEngine.testCaseFailed(engineReceiveMessage);			
			e.printStackTrace();				
		};		
		httpClient.getConnectionManager().shutdown();		
		engine.closeSugar();

	}
	
}