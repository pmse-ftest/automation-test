import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class ChangeField{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;

	private static String sugarUrl = "";
	private static String sugarUser = "";
	private static String sugarPass = "";	
	private static String titleCase = "";	
	private static String lastName = "";	
	private static String engineChangeField = "";

	public static void main(String[] args) throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		engineChangeField = TestEngine.getConfig("engine.changeField");
		sugarUrl = TestEngine.getConfig("sugar.url");
		sugarUser = TestEngine.getConfig("sugar.name");
		sugarPass = TestEngine.getConfig("sugar.password");		
		TestEngine.testCaseClean(engineChangeField);
		
		System.out.println ("Login to SugarCRM...");
		engine.openBrowser(sugarUrl);		
		engine.loginSugar(sugarUser,sugarPass, true);
		System.out.println ("Creating a Lead...");		
		lastName = "Due" + TestEngine.getRandomString(6);
		titleCase = "John " + lastName;
		engine.createLead(lastName, "Change Field");
		engine.logoutSugar();
		try {		

			System.out.println ("Verify Change Field");	
			System.out.println ("-------------------------------");	
			engine.loginSugar(sugarUser,sugarPass, false);	
			System.out.println (engine.getValueLead("title", titleCase));	
			if((engine.getValueLead("title", titleCase).equals("Changes by Change Field")) && (engine.getValueLead("description", titleCase).equals("Changes by Change Field")))
			{
				TestEngine.testCasePassed(engineChangeField);
			}else{
				
				throw new Exception();
			}
			engine.logoutSugar();	
			engine.loginSugar(sugarUser,sugarPass, false);				
			engine.openCase(titleCase);
			engine.updateCase("Approve", "description", "user Administrator");
			engine.logoutSugar();
		}catch (Exception e) {
			TestEngine.testCaseFailed(engineChangeField);		
			e.printStackTrace();				
		};		
		httpClient.getConnectionManager().shutdown();		
		engine.closeSugar();

	}
	
}