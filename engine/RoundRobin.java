import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class RoundRobin{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;

	private static String sugarUrl = "";
	private static String sugarUser = "";
	private static String sugarPass = "";	
	private static String pmseProcess = "";
	private static String titleCase = "";	
	private static String lastName = "";	
	private static String engineRoundRobin = "";

	public static void main(String[] args) throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		sugarUrl = TestEngine.getConfig("sugar.url");
		sugarUser = TestEngine.getConfig("sugar.name");
		sugarPass = TestEngine.getConfig("sugar.password");	
		engineRoundRobin = TestEngine.getConfig("engine.roundRobin");
		
		TestEngine.testCaseClean(engineRoundRobin);
		
		System.out.println ("Login to SugarCRM...");
		engine.openBrowser(sugarUrl);		
		engine.loginSugar(sugarUser,sugarPass, true);
		System.out.println ("Creating a Lead...");		
		lastName = "Due" + TestEngine.getRandomString(6);
		titleCase = "John " + lastName;
		engine.createLead(lastName, "RoundRobin");
		engine.logoutSugar();
		try {		
			System.out.println ("Round Robin");	
			System.out.println ("------------------");	
			System.out.println ("user6 ...");				
			engine.loginSugar("user6",sugarPass, true);	
			engine.openCase(titleCase);				
			engine.derivateCase("Approve");			
			System.out.println ("user 5 ....");	
			System.out.println ("------------------");	
			engine.logoutSugar();
			engine.loginSugar("user5",sugarPass, true);	
			engine.openCase(titleCase);				
			engine.derivateCase("Approve");
			System.out.println ("user 4 ...");	
			System.out.println ("------------------");	
			engine.logoutSugar();
			engine.loginSugar("user4",sugarPass, true);	
			engine.openCase(titleCase);				
			engine.derivateCase("Approve");
			engine.logoutSugar();			
			engine.loginSugar(sugarUser,sugarPass, false);	
			engine.openCase(titleCase);				
			engine.derivateCase("Approve");			
			TestEngine.testCasePassed(engineRoundRobin);

			engine.logoutSugar();
		}catch (Exception e) {
			TestEngine.testCaseFailed(engineRoundRobin);		
			e.printStackTrace();				
		};		
		httpClient.getConnectionManager().shutdown();		
		engine.closeSugar();

	}
	
}