import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class StartEvents{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;

	private static String sugarUrl = "";
	private static String sugarUser = "";
	private static String sugarPass = "";	
	private static String titleCase = "";	
	private static String lastName = "";	
	private static String engineStart = "";

	public static void main(String[] args) throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		sugarUrl = TestEngine.getConfig("sugar.url");
		sugarUser = TestEngine.getConfig("sugar.name");
		sugarPass = TestEngine.getConfig("sugar.password");	
		engineStart = TestEngine.getConfig("engine.start");
		TestEngine.testCaseClean(engineStart);

		System.out.println ("Login to SugarCRM...");
		engine.openBrowser(sugarUrl);		
		engine.loginSugar(sugarUser,sugarPass, true);
		System.out.println ("Creating a Lead...");		
		lastName = "Due" + TestEngine.getRandomString(6);
		titleCase = "John " + lastName;
		engine.createLead(lastName, "Start Events");
		engine.logoutSugar();
		try {		
			engine.loginSugar(sugarUser,sugarPass, false);	
			System.out.println ("Testing Start Events...");	
			engine.openCase(titleCase);			
			engine.derivateCase("Approve");
			TestEngine.testCasePassed(engineStart);
			engine.logoutSugar();
		}catch (Exception e) {
			TestEngine.testCaseFailed(engineStart);		
		};		
		httpClient.getConnectionManager().shutdown();		
		engine.closeSugar();

	}
	
}