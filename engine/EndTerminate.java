import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class EndTerminate{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;
	private static String sugarUrl = "";
	private static String sugarUser = "";
	private static String sugarPass = "";	
	private static String titleCase = "";	
	private static String lastName = "";	
	private static String engineEndTerminate = "";

	public static void main(String[] args) throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		sugarUrl = TestEngine.getConfig("sugar.url");
		sugarUser = TestEngine.getConfig("sugar.name");
		sugarPass = TestEngine.getConfig("sugar.password");	
		engineEndTerminate = TestEngine.getConfig("engine.endTerminate");
			
		TestEngine.testCaseClean(engineEndTerminate);
			
		System.out.println ("Login to SugarCRM...");
		engine.openBrowser(sugarUrl);		
		engine.loginSugar(sugarUser,sugarPass, true);
		System.out.println ("Creating a Lead...");		
		lastName = "Due" + TestEngine.getRandomString(6);
		titleCase = "John " + lastName;
		engine.createLead(lastName, "End-Terminate");
		engine.logoutSugar();
		try {		

			System.out.println ("End Terminate");	
			System.out.println ("----------------------");
			engine.loginSugar("user1",sugarPass, true);
			System.out.println ("user 1....");	
			engine.openCase(titleCase);
			engine.logoutSugar();			
			engine.loginSugar(sugarUser,sugarPass, false);
			System.out.println ("admin....");				
			engine.openCase(titleCase);
			engine.derivateCase("Approve");
			engine.logoutSugar();
			engine.loginSugar("user1",sugarPass, false);
			System.out.println ("user 1....");	
			engine.openCase(titleCase);
			engine.derivateCase("Approve");
			engine.logoutSugar();			
			TestEngine.testCaseFailed(engineEndTerminate);			
		}catch (Exception e) {
			TestEngine.testCasePassed(engineEndTerminate);		
		};		
		httpClient.getConnectionManager().shutdown();		
		engine.closeSugar();
	}
	
}