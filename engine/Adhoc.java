import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class Adhoc{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;

	private static String sugarUrl = "";
	private static String sugarUser = "";
	private static String sugarPass = "";	
	private static String titleCase = "";	
	private static String lastName = "";	
	private static String engineReassign = "";

	public static void main(String[] args) throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		sugarUrl = TestEngine.getConfig("sugar.url");
		sugarUser = TestEngine.getConfig("sugar.name");
		sugarPass = TestEngine.getConfig("sugar.password");	
		engineReassign = TestEngine.getConfig("engine.reassign");	
		
		TestEngine.testCaseClean(engineReassign);
		
		System.out.println ("Login to SugarCRM...");
		engine.openBrowser(sugarUrl);		
		engine.loginSugar(sugarUser,sugarPass, true);
		System.out.println ("Creating a Lead...");		
		lastName = "Due" + TestEngine.getRandomString(6);
		titleCase = "John " + lastName;
		engine.createLead(lastName, "Adhoc");
		engine.logoutSugar();
		engine.loginSugar(sugarUser,sugarPass, false);		
		try {		
			System.out.println ("Adhoc");	
			System.out.println ("-------------------------------");				
			engine.openCase(titleCase);
			System.out.println ("Reassign Case (Adhoc)");	
			System.out.println ("-------------------------------");				
			engine.reassignCase("user1", "One Way");
			engine.logoutSugar();
			System.out.println ("user 1 ...");				
			engine.loginSugar("user1",sugarPass, true);
			engine.openCase(titleCase);
			System.out.println ("user 2 ...");				
			engine.reassignCase("user2", "One Way");	
			engine.logoutSugar();
			engine.loginSugar("user2",sugarPass,true);
			engine.openCase(titleCase);	
			engine.derivateCase("Route");
			System.out.println ("admin ...");				
			engine.logoutSugar();
			engine.loginSugar(sugarUser,sugarPass, false);	
			engine.openCase(titleCase);				
			engine.derivateCase("Approve");	
			TestEngine.testCasePassed(engineReassign);
			engine.logoutSugar();
		}catch (Exception e) {
			TestEngine.testCaseFailed(engineReassign);		
			e.printStackTrace();				
		};		
		httpClient.getConnectionManager().shutdown();		
		engine.closeSugar();

	}
	
}