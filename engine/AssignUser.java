import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class AssignUser{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;

	private static String sugarUrl = "";
	private static String sugarUser = "";
	private static String sugarPass = "";	
	private static String titleCase = "";	
	private static String lastName = "";	
	private static String engineAssignUser = "";

	public static void main(String[] args) throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		sugarUrl = TestEngine.getConfig("sugar.url");
		sugarUser = TestEngine.getConfig("sugar.name");
		sugarPass = TestEngine.getConfig("sugar.password");			
		engineAssignUser = TestEngine.getConfig("engine.assignUser");
		
		TestEngine.testCaseClean(engineAssignUser);
		
		System.out.println ("Login to SugarCRM...");
		engine.openBrowser(sugarUrl);		
		engine.loginSugar(sugarUser,sugarPass, true);
		System.out.println ("Creating a Lead...");		
		lastName = "Due" + TestEngine.getRandomString(6);
		titleCase = "John " + lastName;
		engine.createLead(lastName, "Assign User");
		engine.logoutSugar();
		try {		
			engine.loginSugar(sugarUser,sugarPass, false);
			engine.openCase(titleCase);
			engine.derivateCase("Approve");
			engine.logoutSugar();
			engine.loginSugar("user9",sugarPass, true);	
			System.out.println ("user 9 ...");	
			engine.openCase(titleCase);				
			engine.updateCase("Approve", "department", "sales");			
			engine.logoutSugar();
			TestEngine.testCasePassed(engineAssignUser);
		}catch (Exception e) {
			TestEngine.testCaseFailed(engineAssignUser);		
			e.printStackTrace();				
		};		
		httpClient.getConnectionManager().shutdown();		
		engine.closeSugar();

	}
	
}