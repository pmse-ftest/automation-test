import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class ExecuteEngine{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;

	private static String testCaseStartEvents = "";
	private static String testIntermediateEvents = "";
	private static String testCaseScriptTasks = "";
	private static String testCaseGateways = "";
	private static String testCaseEndEvents = "";
	private static String testCaseEndDelete = "";
	private static String testCaseDrawing = "";	
	private static String sugarUrl = "";
	private static String sugarUser = "";
	private static String sugarPass = "";	
	private static String pmseProcess = "";
	private static String pmseEmaiTemplate = "";
	private static String pmseBusinessRule = "";	
	private static String titleCase = "";	
	private static String lastName = "";	
	private static String engineParallelConverging = "";
	private static String engineParallelDiverging = "";
	private static String engineInclusive = "";
	private static String engineExclusiveDiverging = "";
	private static String engineStart = "";
	private static String engineEndTerminate = "";
	private static String engineEventBased = "";
	private static String engineChangeField = "";
	private static String engineBusinessRule = "";
	private static String engineAssignUser = "";
	private static String engineReceiveMessage = "";
	private static String engineRoundRobin = "";
	private static String engineAddRelated = "";
	private static String engineSelfService = "";
	private static String engineChangeOwner = "";
	private static String engineReassign = "";
	private static String currentTestCase = "";		

	public static void main(String[] args) throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		testCaseStartEvents = TestEngine.getConfig("jira.start");
		testIntermediateEvents = TestEngine.getConfig("jira.intermediate");
		testCaseScriptTasks = TestEngine.getConfig("jira.tasks");
		testCaseGateways = TestEngine.getConfig("jira.gateways");
		testCaseEndEvents = TestEngine.getConfig("jira.end");
		testCaseEndDelete = TestEngine.getConfig("jira.delete");
		testCaseDrawing = TestEngine.getConfig("jira.draw");	
		sugarUrl = TestEngine.getConfig("sugar.url");
		sugarUser = TestEngine.getConfig("sugar.name");
		sugarPass = TestEngine.getConfig("sugar.password");	
		pmseProcess = TestEngine.getConfig("pmse.process");
		pmseEmaiTemplate = TestEngine.getConfig("pmse.emailtemplate");
		pmseBusinessRule = TestEngine.getConfig("pmse.businessrule");
		engineParallelConverging = TestEngine.getConfig("engine.parallelConverging");
		engineParallelDiverging = TestEngine.getConfig("engine.parallelDiverging");
		engineInclusive = TestEngine.getConfig("engine.inclusive");
		engineExclusiveDiverging = TestEngine.getConfig("engine.exclusiveDiverging");
		engineStart = TestEngine.getConfig("engine.start");
		engineEndTerminate = TestEngine.getConfig("engine.endTerminate");
		engineEventBased = TestEngine.getConfig("engine.eventBased");
		engineChangeField = TestEngine.getConfig("engine.changeField");
		engineBusinessRule = TestEngine.getConfig("engine.businessRule");
		engineAssignUser = TestEngine.getConfig("engine.assignUser");
		engineReceiveMessage = TestEngine.getConfig("engine.receiveMessage");
		engineRoundRobin = TestEngine.getConfig("engine.roundRobin");
		engineAddRelated = TestEngine.getConfig("engine.addRelated");
		engineSelfService = TestEngine.getConfig("engine.selfService");
		engineChangeOwner = TestEngine.getConfig("engine.changeOwner");
		engineReassign = TestEngine.getConfig("engine.reassign");	
		
		TestEngine.testCaseClean(engineParallelConverging);
		TestEngine.testCaseClean(engineParallelDiverging);
		TestEngine.testCaseClean(engineInclusive);
		TestEngine.testCaseClean(engineExclusiveDiverging);
		TestEngine.testCaseClean(engineStart);
		TestEngine.testCaseClean(engineEndTerminate);
		TestEngine.testCaseClean(engineEventBased);
		TestEngine.testCaseClean(engineChangeField);
		TestEngine.testCaseClean(engineBusinessRule);
		TestEngine.testCaseClean(engineAssignUser);
		TestEngine.testCaseClean(engineReceiveMessage);
		TestEngine.testCaseClean(engineChangeOwner);
		TestEngine.testCaseClean(engineRoundRobin);
		TestEngine.testCaseClean(engineAddRelated);
		TestEngine.testCaseClean(engineSelfService);
		TestEngine.testCaseClean(engineReassign);
		
		System.out.println ("Login to SugarCRM...");
		engine.openBrowser(sugarUrl);		
		engine.loginSugar(sugarUser,sugarPass, true);
		System.out.println ("Creating a Lead...");		
		lastName = "Due" + TestEngine.getRandomString(6);
		titleCase = "John " + lastName;
		engine.createLead(lastName, "Automation Test");
		engine.logoutSugar();
		engine.loginSugar("user7",sugarPass, true);		
		try {		
			System.out.println ("Claim case");	
			System.out.println ("-------------------------------");				
			engine.claimCase(titleCase);
			currentTestCase = engineSelfService;
			TestEngine.testCasePassed(engineSelfService);			
			TestEngine.testCasePassed(engineStart);			
	/*		System.out.println ("Change Owner");	
			System.out.println ("-------------------------------");				
			engine.changeOwner("user3");
			currentTestCase = engineChangeOwner;
			if(engine.getUserRecord(titleCase) == "user3")
			{
				TestEngine.testCasePassed(engineChangeOwner);
			}else{
				TestEngine.testCaseFailed(engineChangeOwner);
				throw new Exception();				
			}
			engine.goHome();
			engine.openCase(titleCase);
			System.out.println ("Reassign Case (Adhoc)");	
			System.out.println ("-------------------------------");				
			engine.reassignCase("user1", "One Way");
			engine.logoutSugar();
			System.out.println ("user 1 ...");				
			engine.loginSugar("user1",sugarPass, true);
			engine.openCase(titleCase);
			System.out.println ("user 2 ...");				
			engine.reassignCase("user2", "One Way");	
			engine.logoutSugar();
			engine.loginSugar("user2",sugarPass,true);
			engine.openCase(titleCase);	
			engine.derivateCase("Route");
			System.out.println ("user 7 ...");				
			engine.logoutSugar();
			engine.loginSugar("user7",sugarPass, false);	*/	
			engine.derivateCase("Approve");	
			//currentTestCase = engineReassign;			
			//TestEngine.testCasePassed(engineReassign);
			engine.logoutSugar();
			System.out.println ("Verify Change Field");	
			System.out.println ("-------------------------------");	
			engine.loginSugar(sugarUser,sugarPass, false);	
			System.out.println (engine.getValueLead("title", titleCase));	
			System.out.println (engine.getValueLead("description", titleCase));	
			currentTestCase = engineExclusiveDiverging;				
			if((engine.getValueLead("title", titleCase).equals("Changes by Change Field")) && (engine.getValueLead("description", titleCase).equals("Changes by Change Field")))
			{
				TestEngine.testCasePassed(engineExclusiveDiverging);			
				TestEngine.testCasePassed(engineChangeField);
			}else{
				
				TestEngine.testCaseFailed(engineChangeField);
				throw new Exception();
			}
	
			System.out.println ("Verify Add Related Record");	
			System.out.println ("-------------------------------");		
			engine.closeTask("Test tasks",titleCase);
			currentTestCase = engineAddRelated;			
			TestEngine.testCasePassed(engineAddRelated);
			engine.logoutSugar();
			engine.loginSugar("user7",sugarPass, false);
			System.out.println ("Current User and Route response");	
			System.out.println ("---------------------------------------");		
			engine.openCase(titleCase);
			engine.derivateCase("Route");
			currentTestCase = engineReceiveMessage;				
			TestEngine.testCasePassed(engineReceiveMessage);
			TestEngine.testCasePassed(engineEventBased);
			System.out.println ("Parallel diverging");	
			System.out.println ("----------------------");		
			engine.logoutSugar();
			engine.loginSugar("user5",sugarPass, true);
			System.out.println ("Record Owner and Static assignment");	
			System.out.println ("-----------------------------------------");		
			engine.openCase(titleCase);
			engine.derivateCase("Approve");
			engine.logoutSugar();
			engine.loginSugar(sugarUser,sugarPass, false);
			currentTestCase = engineParallelDiverging;				
			TestEngine.testCasePassed(engineParallelDiverging);
			System.out.println ("Parallel converging");	
			System.out.println ("----------------------");
			System.out.println ("Update Showcase and Approve");	
			System.out.println ("---------------------------");	
			engine.openCase(titleCase);
			engine.updateCase("Approve", "description", "user Administrator");
			currentTestCase = engineParallelConverging;				
			TestEngine.testCasePassed(engineParallelConverging);			
			engine.openCase(titleCase);
			engine.derivateCase("Approve");
			System.out.println ("Round Robin");	
			System.out.println ("------------------");			
			engine.logoutSugar();
			engine.loginSugar("user6",sugarPass, true);	
			engine.openCase(titleCase);				
			engine.updateCase("Approve", "department", "sales");			
			System.out.println ("user 5 ....");	
			System.out.println ("------------------");	
			engine.logoutSugar();
			engine.loginSugar("user5",sugarPass, false);	
			engine.openCase(titleCase);				
			engine.updateCase("Approve", "description", "user5");
			System.out.println ("user 4 ...");	
			System.out.println ("------------------");	
			engine.logoutSugar();
			engine.loginSugar("user4",sugarPass, true);	
			engine.openCase(titleCase);				
			engine.updateCase("Approve", "description", "user4");
			currentTestCase = engineRoundRobin;				
			TestEngine.testCasePassed(engineRoundRobin);
			System.out.println ("Inclusive Gateway");	
			System.out.println ("-------------------");	
			engine.logoutSugar();
			engine.loginSugar("user2",sugarPass, true);
			engine.openCase(titleCase);				
			engine.updateCase("Approve", "department", "sales");			
			System.out.println ("user admin....");	
			System.out.println ("-------------------");	
			engine.logoutSugar();
			engine.loginSugar(sugarUser,sugarPass, false);	
			engine.openCase(titleCase);			
			engine.derivateCase("Approve");
			currentTestCase = engineInclusive;	
			TestEngine.testCasePassed(engineInclusive);
			System.out.println ("Business Rule and Assign User");	
			System.out.println ("-----------------------------");			
			engine.logoutSugar();
			engine.loginSugar("user9",sugarPass, true);	
			engine.openCase(titleCase);				
			engine.derivateCase("Approve");	
			currentTestCase = engineBusinessRule;				
			TestEngine.testCasePassed(engineBusinessRule);
			TestEngine.testCasePassed(engineAssignUser);
			System.out.println ("Change Field to Related Record");	
			System.out.println ("-----------------------------");			
			engine.logoutSugar();
			engine.loginSugar("user3",sugarPass, true);	
			engine.openRelatedRecord("Test tasks",titleCase);	
			currentTestCase = engineChangeField;			
			if(engine.getValueLead("description", titleCase).equals("Changes by Change Field"))
			{
				TestEngine.testCasePassed(engineChangeField);
			}else{
				TestEngine.testCaseFailed(engineChangeField);
				throw new Exception();
			}
	
			engine.logoutSugar();
		}catch (Exception e) {
			TestEngine.testCaseFailed(currentTestCase);		
			e.printStackTrace();				
		};		
		httpClient.getConnectionManager().shutdown();		
		engine.closeSugar();

	}
	
}