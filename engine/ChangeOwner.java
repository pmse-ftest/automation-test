import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class ChangeOwner{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;

	private static String sugarUrl = "";
	private static String sugarUser = "";
	private static String sugarPass = "";	
	private static String titleCase = "";	
	private static String lastName = "";	
	private static String engineChangeOwner = "";

	public static void main(String[] args) throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		sugarUrl = TestEngine.getConfig("sugar.url");
		sugarUser = TestEngine.getConfig("sugar.name");
		sugarPass = TestEngine.getConfig("sugar.password");	
		engineChangeOwner = TestEngine.getConfig("engine.changeOwner");
		
		TestEngine.testCaseClean(engineChangeOwner);
		
		System.out.println ("Login to SugarCRM...");
		engine.openBrowser(sugarUrl);		
		engine.loginSugar(sugarUser,sugarPass, true);
		System.out.println ("Creating a Lead...");		
		lastName = "Due" + TestEngine.getRandomString(6);
		titleCase = "John " + lastName;
		engine.createLead(lastName, "Change Owner");
		engine.logoutSugar();
		engine.loginSugar(sugarUser,sugarPass, false);		
		try {		
			
			System.out.println ("Change Owner");	
			System.out.println ("-------------------------------");	
			engine.openCase(titleCase);			
			engine.changeOwner("user3");
			engine.getUserRecord(titleCase, "user3");
			engine.logoutSugar();
			engine.loginSugar(sugarUser,sugarPass, false);
			engine.openCase(titleCase);
			engine.updateCase("Approve", "description", "user Administrator");	
			engine.logoutSugar();
			TestEngine.testCasePassed(engineChangeOwner);
		}catch (Exception e) {
			TestEngine.testCaseFailed(engineChangeOwner);		
			e.printStackTrace();				
		};		
		httpClient.getConnectionManager().shutdown();		
		engine.closeSugar();

	}
	
}