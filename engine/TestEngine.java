import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import java.util.List;
import org.apache.http.entity.StringEntity;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;
import javax.naming.AuthenticationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.openqa.selenium.ie.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import java.util.Random;


public class TestEngine{
	public static TestEngine engine = new TestEngine();
	public static WebDriver driver = null;


	// This method open browser with given instance
	public void openBrowser(String instance)throws IOException{
		setConfig();
		switch (getConfig("browser"))
		{
			case "FireFox":
				//driver = new  FirefoxDriver();
				ProfilesIni profile = new ProfilesIni();
				FirefoxProfile ffprofile = profile.getProfile("SELENIUM");
				driver = new FirefoxDriver(ffprofile);
				break;
			case "Internet Explorer":
				System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
				driver = new  InternetExplorerDriver();
				break;
			default:
				System.setProperty("webdriver.chrome.driver", "chromedriver209.exe");			
				driver = new ChromeDriver();
				break;
		}
		//driver.manage().timeouts().setScriptTimeout(6000,TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement sugarInstance = null;
		driver.manage().window().maximize();
		driver.get(instance);
	}
	
	// This method login to SugarCRM
	public void loginSugar(String user, String password, boolean done)throws IOException, Exception{
		System.out.println ("Login to SugarCRM.....");
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("username")));
		myDynamicElement.sendKeys(user);
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.name("login_button")).click();

		if (done == true)
		{
			WebElement myDynamicElement2 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Done")));	
			myDynamicElement2.click();
		}
		Thread.sleep(5000);		
	}
	
	// This method logout to SugarCRM
	public void logoutSugar()throws Exception{
		System.out.println ("Log Out from SugarCRM.....");
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("#userTab")).click();		
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Log Out")));		
		myDynamicElement.click();	
		Thread.sleep(5000);		
	}

	// This method close the browser
	public void closeSugar(){
		driver.close();	
		driver.quit();	
	}

	// This method create a Lead
	public void createLead(String lastName, String description)throws IOException{
		
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Leads")));
		myDynamicElement.click();
		WebElement myDynamicElement2 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("create_button")));
		myDynamicElement2.click();	
		WebElement myDynamicElement3 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("first_name")));		
		myDynamicElement3.sendKeys("John");
		driver.findElement(By.name("last_name")).sendKeys(lastName);	
		WebElement myDynamicElement4 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#drawers > div > div > div.main-pane.span8 > div > div > div > div.row-fluid.show-hide-toggle > div > button.btn-link.btn-invisible.more")));			
		myDynamicElement4.click();	
		driver.findElement(By.name("department")).sendKeys("Sales");
		driver.findElement(By.name("description")).sendKeys(description);		
		driver.findElement(By.cssSelector("#drawers > div > div > div.main-pane.span8 > div > div > div > div.headerpane > h1 > div > span.actions.btn-group.detail > span > a")).click();
	}

	// This method claim a case with the given Title case
	public void claimCase(String titleCase)throws IOException, Exception{
		Thread.sleep(5000);
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Self Service")));		
		myDynamicElement.click();	
		WebElement myDynamicElement1 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText(titleCase)));		
		myDynamicElement1.click();	
		WebElement myDynamicElement2 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Claim")));		
		myDynamicElement2.click();	
		
	}

	// This method open a case with the given Title case
	public void openCase(String titleCase)throws IOException, Exception{
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText(titleCase)));		
		myDynamicElement.click();
		Thread.sleep(5000);		
	}
	
	// This method open a case with existing multiple cases
	public void openCaseMulti(String titleCase)throws IOException, Exception{
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#content > div > div > div > div > ul > li.span8 > ul > li:nth-child(1) > div > ul.dashlet-cell.rows.row-fluid > li > div > div > div.dashlet-content > div > div.dashlet-unordered-list > div.tab-content > div > ul > li:nth-child(1) > p > a")));		
		myDynamicElement.click();
		Thread.sleep(5000);		
	}	
	
	// This method claim change the record owner
	public void changeOwner(String newOwner)throws IOException, Exception{
		Thread.sleep(5000);		
		driver.findElement(By.cssSelector("#content > div > div > div.main-pane.span8 > div > div:nth-child(1) > div > div.headerpane > div:nth-child(2) > h1 > div > span.actions.btn-group.detail > a")).click();	
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("find_duplicates_button")));		
		myDynamicElement.click();	
		WebElement element = driver.findElement(By.id("reassign_user"));		
		Select droplist2 = new Select(element);
		droplist2.selectByVisibleText(newOwner);	
		Thread.sleep(5000);			
		driver.findElement(By.partialLinkText("Save")).click();	
	}	

	// This method claim change the record owner
	public void reassignCase(String newOwner, String type)throws IOException, Exception{
		Thread.sleep(5000);		
		driver.findElement(By.cssSelector("#content > div > div > div.main-pane.span8 > div > div:nth-child(1) > div > div.headerpane > div:nth-child(2) > h1 > div > span.actions.btn-group.detail > a")).click();	
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("duplicate_button")));		
		myDynamicElement.click();	
		WebElement element = driver.findElement(By.id("adhoc_user"));		
		Select droplist2 = new Select(element);
		droplist2.selectByVisibleText(newOwner);	
		element = driver.findElement(By.id("adhoc_type"));		
		Select droplist3 = new Select(element);
		droplist3.selectByVisibleText(type);	
		Thread.sleep(5000);			
		driver.findElement(By.partialLinkText("Save")).click();	
	}
	
	// This method get the user owner from record lead
	public void getUserRecord(String recordName, String userName)throws IOException, Exception{
		Thread.sleep(5000);		
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Leads")));
		myDynamicElement.click();
		WebElement myDynamicElement2 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(recordName)));
		myDynamicElement2.click();
//		WebElement myDynamicElement3 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("edit_button")));
//		myDynamicElement3.click();		
//		WebElement myDynamicElement4 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.id("#content > div > div > div.main-pane.span8 > div > div:nth-child(1) > div > div.row-fluid.show-hide-toggle > div > button.btn-link.btn-invisible.more")));			
//		myDynamicElement4.click();			
		WebElement element = driver.findElement(By.linkText(userName));		
	
	}	
	
	// This method derivate case (Approve/Reject/Route)
	public void derivateCase(String response)throws IOException, Exception{
		Thread.sleep(5000);		
		String nameResponse = "";
		switch (response)
		{
			case "Approve":
				nameResponse = "approve_button";
				break;
			default:
				nameResponse = "reject_button";
				break;
		}
		
		WebElement myDynamicElement3 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name(nameResponse)));
		myDynamicElement3.click();		
	}	

	// This method get the value from record lead
	public String getValueLead(String field, String recordName)throws IOException, Exception{
		Thread.sleep(5000);	
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Leads")));
		myDynamicElement.click();
		WebElement myDynamicElement2 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(recordName)));
		myDynamicElement2.click();
		Thread.sleep(5000);			
		WebElement myDynamicElement3 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("edit_button")));
		myDynamicElement3.click();		
		return  driver.findElement(By.name(field)).getAttribute("value");	
	}	

	
	// This method open a related record given the record title and close it
	public void closeTask(String relatedName, String recordName)throws IOException, Exception{
		Thread.sleep(15000);	
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Leads")));
		myDynamicElement.click();
		//WebElement myDynamicElement2 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(recordName)));
		//myDynamicElement2.click();
		Thread.sleep(5000);			
		System.out.println ("Open task...");	
		WebElement myDynamicElement3 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(relatedName)));
		myDynamicElement3.click();	
		Thread.sleep(5000);	
		driver.findElement(By.cssSelector("a.btn-primary:nth-child(2)")).click();	
		WebElement myDynamicElement4 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("record-close")));		
		myDynamicElement4.click();		
		
	}		

	// This method open a related record
	public void openRelatedRecord(String relatedName, String recordName)throws IOException{
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Leads")));
		myDynamicElement.click();
		WebElement myDynamicElement2 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(recordName)));
		myDynamicElement2.click();
		WebElement myDynamicElement3 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(relatedName)));
		myDynamicElement3.click();	
	}	
	
	// This method update the current open Showcase
	public void updateCase(String response, String field, String value)throws IOException, Exception{
		Thread.sleep(5000);	
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("edit_button")));
		myDynamicElement.click();	
		driver.findElement(By.name(field)).clear();		
		driver.findElement(By.name(field)).sendKeys(value);
		String nameResponse = "";
		switch (response)
		{
			case "Approve":
				nameResponse = "approve_button";
				break;
			default:
				nameResponse = "reject_button";
				break;
		}
		Thread.sleep(5000);	
		driver.findElement(By.name(nameResponse)).click();		
	
		
	}	

	// This method redirecto to home page SugarCRM
	public void goHome()throws IOException{
		driver.findElement(By.cssSelector(".module-list > ul:nth-child(1) > li:nth-child(1) > span:nth-child(1) > a:nth-child(1)")).click();	
	}	

	
	// This method get a random string
	public static String getRandomString (int longString){
		String randomString = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while ( i < longString){
			char c = (char)r.nextInt(255);
			if ( (c >= '0' && c <='9') || (c >='A' && c <='Z') ){
				randomString += c;
				i ++;
			}
		}
		return randomString;
	}
	
	// This method update Jira with the given data
	public static void updateJira(String auth, String url, String data) throws AuthenticationException, ClientHandlerException, IOException {
		setConfig();
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").put(ClientResponse.class, data);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
	        else if(response.getStatus() == 403)
                throw new ClientHandlerException("Error " + response.getStatus());
            else if(response.getStatus() != 204) 
                throw new ClientHandlerException("Error " + response.getStatus());		
	}	

	// This method update the Test Case as Passed
	public static void testCasePassed(String testCaseId) throws AuthenticationException, ClientHandlerException, IOException{
		setConfig();
		updateJira(jiraAuth, jiraUrl + testCaseId, issueStatusPassed);
		System.out.println ("Test case "+ testCaseId + " has been PASSED");		
	}	

	// This method set the test case to Can't Test
	public static void testCaseClean(String testCaseId) throws AuthenticationException, ClientHandlerException, IOException{
		setConfig();
		System.out.println ("Set Test case " + testCaseId + " to Can't Test");
		updateJira(jiraAuth, jiraUrl + testCaseId, issueStatusNoTest);
	}	
	
	// This method update the Test Case as Failed
	public static void testCaseFailed(String testCaseId)throws AuthenticationException, ClientHandlerException, IOException{
		setConfig();
		updateJira(jiraAuth, jiraUrl + testCaseId, issueStatusFailed);
		updateJira(jiraAuth, jiraUrl + testCaseId, issueComment);
		System.out.println ("Test case "+ testCaseId + " has been FAILED!!!!");		
	}	

	// This method get the config from config file	
	public static String getConfig(String propertyName)throws IOException{
		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream("default.conf");
 		prop.load(input);
		String result = prop.getProperty(propertyName);
		return result;
	}

	// This method get the config from config file and set the variables	
	public static void setConfig()throws IOException{
		jiraUrl = getConfig("jira.url");
		jiraAuth = new String(Base64.encode(getConfig("jira.user") + ":" + getConfig("jira.password")));
		issueStatusFailed = "{\"fields\": {\"" + getConfig("jira.custom") + "\":\"Failed\"}}";
		issueStatusPassed = "{\"fields\": {\"" + getConfig("jira.custom") + "\":\"Passed\"}}";	
		issueStatusNoTest = "{\"fields\": {\"" + getConfig("jira.custom") + "\":\"Can't Test\"}}";			
		issueComment = "{\"update\": {\"comment\":[{\"add\": {\"body\": \"The user is not able to use this element\"}}]}}";	
		
	}
	
	private static String jiraUrl = "";
	private static String jiraAuth = "";
	private static String issueStatusFailed = "";
	private static String issueStatusPassed = "";	
	private static String issueStatusNoTest = "";	
	private static String issueComment = "";	

}