import org.openqa.selenium.WebDriver;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
public class TestDesignerExecute{
	public static TestDesigner designer = new TestDesigner();
	public static WebDriver driver = null;
	public static String testCaseStartEvents = "";
	public static String testIntermediateEvents = "";
	public static String testCaseScriptTasks = "";
	public static String testCaseGateways = "";
	public static String testCaseEndEvents = "";
	public static String testCaseEndDelete = "";
	public static String testCaseDrawing = "";	
	public static String sugarUrl = "";
	public static String sugarUser = "";
	public static String sugarPass = "";	
	public static String pmseProcess = "";
	public static String pmseEmaiTemplate = "";
	public static String pmseBusinessRule = "";
	
	public static void main(String[] args) throws Exception{
	
		testCaseStartEvents = TestDesigner.getConfig("designer.start");
		testIntermediateEvents = TestDesigner.getConfig("designer.intermediate");
		testCaseScriptTasks = TestDesigner.getConfig("designer.tasks");
		testCaseGateways = TestDesigner.getConfig("designer.gateways");
		testCaseEndEvents = TestDesigner.getConfig("designer.end");
		testCaseEndDelete = TestDesigner.getConfig("designer.delete");
		testCaseDrawing = TestDesigner.getConfig("designer.draw");	
		sugarUrl = TestDesigner.getConfig("sugar.url");
		sugarUser = TestDesigner.getConfig("sugar.name");
		sugarPass = TestDesigner.getConfig("sugar.password");	
		pmseProcess = TestDesigner.getConfig("pmse.process");
		pmseEmaiTemplate = TestDesigner.getConfig("pmse.emailtemplate");
		pmseBusinessRule = TestDesigner.getConfig("pmse.businessrule");	
		HttpClient httpClient = new DefaultHttpClient();
		
		System.out.println ("Login to SugarCRM...");
		designer.loginSugar(sugarUser,sugarPass,sugarUrl);
		
		System.out.println ("Opening the process...");		
		designer.openProcess(pmseProcess);
		
		try {
			System.out.println ("Testing Start Events...");	
			System.out.println ("-------------------------------");				
			designer.createAction("Start Event Lead", 50, 50);
			designer.createAction("Start Event Opportunity", 100, 50);
			designer.createAction("Start Event Document", 150, 50);
			designer.createAction("Other Module Event", 200, 50);
			designer.setStartEvent("Lead Start Event # 1", "New records only", "Department", "QA");
			designer.setStartEvent("Opportunity Start Event # 1", "New records only", "Opportunity Name", "Dreams INC");
			designer.setStartEvent("Document Start Event # 1", "New records only", "Document Name", "Test cases list");
			designer.setStartEvent("Other Module Event # 1", "New records only", "Department", "QA");
			TestDesigner.testCasePassed(testCaseStartEvents);
		}catch (Exception e) {
			TestDesigner.testCaseFailed(testCaseStartEvents);			
		};		
			
		try {		
			System.out.println ("Testing Intermediate Events...");	
			System.out.println ("-------------------------------");				
			designer.createAction("Wait", 0, 140);
			designer.createAction("Receive Message", 50, 140);
			designer.createAction("Send Message", 100, 140);	
			designer.setTimerEvent("Wait Event # 1", "8", "Hours");
			designer.setReceiveMessage("Message Event # 1", "Department", "QA");
			designer.setSendMessage("Message Event # 2", pmseEmaiTemplate, "admin@test.com");
			designer.saveProcess();				
			TestDesigner.testCasePassed(testIntermediateEvents);			
		}catch (Exception e) {
			TestDesigner.testCaseFailed(testIntermediateEvents);			
		};		

		try {			
			System.out.println ("Testing Script tasks...");	
			System.out.println ("-------------------------------");				
			designer.createAction("User task", -50, 230);
			designer.createAction("Action", 50, 230);
			designer.createAction("Action", 150, 230);
			designer.createAction("Action", 250, 230);
			designer.createAction("Action", 350, 230);
			designer.createAction("Action", 450, 230);	
			designer.changeActionType("Action # 1", "Business Rule");			
			designer.changeActionType("Action # 2", "Assign User");	
			designer.changeActionType("Action # 3", "Round Robin");	
			designer.changeActionType("Action # 4", "Change Field");	
			designer.changeActionType("Action # 5", "Add Related Record");
			designer.saveProcess();			
			//designer.setUserTask("Task # 1", "8", "Hours");		
			designer.setBusinessRule("Action # 1", pmseBusinessRule);
			designer.setAssignUser("Action # 2", "Administrator");
			designer.setRoundRobin("Action # 3", "Global");
			designer.setChangeField("Action # 4");		
			designer.setAddRelatedRecord("Action # 5");
			TestDesigner.testCasePassed(testCaseScriptTasks);			
		}catch (Exception e) {
			TestDesigner.testCaseFailed(testCaseScriptTasks);			
		};

		try {		
			System.out.println ("Testing Gateway...");	
			System.out.println ("-------------------------------");				
			designer.createAction("Exclusive", -100, 310);
			designer.createAction("Exclusive", 100, 310);
			designer.createAction("Exclusive", 250, 310);
			designer.createAction("Exclusive", 400, 310);
			designer.convertGateway("Exclusive Gateway # 2", "Parallel Gateway");			
			designer.convertGateway("Exclusive Gateway # 3", "Inclusive Gateway");	
			designer.convertGateway("Exclusive Gateway # 4", "Event-Based Gateway");
			designer.createAction("User task", 50, 380);
			designer.createAction("User task", 400, 380);
			designer.drawFlow("Exclusive Gateway # 1", "Action # 2");
			designer.drawFlow("Exclusive Gateway # 1", "Task # 2");
			designer.drawFlow("Exclusive Gateway # 3", "Action # 5");
			designer.drawFlow("Exclusive Gateway # 3", "Task # 3");
			designer.saveProcess();		
			designer.setGateways("Exclusive Gateway # 1");
			designer.setGateways("Exclusive Gateway # 3");
			TestDesigner.testCasePassed(testCaseGateways);			
		}catch (Exception e) {
			TestDesigner.testCaseFailed(testCaseGateways);			
		};

		try {		
			System.out.println ("Testing End events and Comment...");	
			System.out.println ("-------------------------------");				
			designer.createAction("End", -150, 450);
			designer.createAction("End", -50, 450);
			designer.createAction("End", 50, 450);
			//designer.createAction("Comment", 150, 450);	
			designer.saveProcess();
			designer.endEventsResult("End Event # 2", "Send Message");
			designer.endEventsResult("End Event # 3", "Terminate Process");
			designer.setSendMessage("End Event # 2", pmseEmaiTemplate, "admin@test.com");
			TestDesigner.testCasePassed(testCaseEndEvents);			
		}catch (Exception e) {
			TestDesigner.testCaseFailed(testCaseEndEvents);			
		};

		try {		
			System.out.println ("Delete all elements...");	
			System.out.println ("-------------------------------");				
			designer.saveProcess();
			System.out.println ("Close designer...");	
			//designer.closeDesigner();
			System.out.println ("Open process...");	
			//designer.openProcess(pmseProcess);
			designer.deleteAction("Lead Start Event # 1");	
			designer.deleteAction("Opportunity Start Event # 1");
			designer.deleteAction("Document Start Event # 1");
			designer.deleteAction("Other Module Event # 1");
			designer.deleteAction("Wait Event # 1");
			designer.deleteAction("Message Event # 1");
			designer.deleteAction("Message Event # 2");
			designer.deleteAction("Task # 1");
			designer.deleteAction("Task # 2");
			designer.deleteAction("Task # 3");		
			designer.deleteAction("Action # 1");
			designer.deleteAction("Action # 2");
			designer.deleteAction("Action # 3");
			designer.deleteAction("Action # 4");
			designer.deleteAction("Action # 5");
			designer.deleteAction("Exclusive Gateway # 1");
			designer.deleteAction("Exclusive Gateway # 2");
			designer.deleteAction("Exclusive Gateway # 3");
			designer.deleteAction("Exclusive Gateway # 4");
			designer.deleteAction("End Event # 1");
			designer.deleteAction("End Event # 2");
			designer.deleteAction("End Event # 3");
			//designer.deleteAction("Text Annotation # 1");
			TestDesigner.testCasePassed(testCaseEndDelete);			
		}catch (Exception e) {
		e.printStackTrace();	
			TestDesigner.testCaseFailed(testCaseEndDelete);			
		};

		try {		
			System.out.println ("Drawing a process...");	
			System.out.println ("-------------------------------");				
			designer.createAction("Start Event Lead", 50, 230);	
			designer.createAction("Action", -150, 230);	
			designer.createAction("Exclusive", -75, 230);	
			designer.createAction("Send Message", 75, 140);	
			designer.createAction("Action", 200, 330);	
			designer.createAction("Wait", 250, 175);
			designer.createAction("Receive Message", 230, 330);
			designer.createAction("Exclusive", 230, 330);
			designer.createAction("Exclusive", 330, 230);
			designer.createAction("End", 260, 370);
			designer.createAction("End", 230, 100);
			designer.createAction("User task", 700, 120);		
			designer.createAction("User task", 700, 230);
			designer.createAction("User task", 700, 340);		
			designer.createAction("Action", 550, 230);
			designer.createAction("Exclusive", 750, 230);	
			designer.createAction("End", 760, 230);
			designer.changeActionType("Action # 1", "Add Related Record");			
			designer.changeActionType("Action # 2", "Business Rule");			
			designer.changeActionType("Action # 3", "Change Field");	
			designer.convertGateway("Exclusive Gateway # 1", "Event-Based Gateway");	
			designer.drawFlow("Lead Start Event # 1", "Action # 1");
			designer.drawFlow("Action # 1", "Exclusive Gateway # 1");
			designer.drawFlow("Exclusive Gateway # 1", "Wait Event # 1");
			designer.drawFlow("Exclusive Gateway # 1", "Message Event # 2");
			designer.drawFlow("Wait Event # 1", "Message Event # 1");
			designer.drawFlow("Message Event # 1", "Exclusive Gateway # 1");
			designer.drawFlow("Message Event # 2", "Action # 2");
			designer.drawFlow("Action # 2", "Exclusive Gateway # 2");
			designer.drawFlow("Exclusive Gateway # 2", "End Event # 1");
			designer.drawFlow("Exclusive Gateway # 2", "Exclusive Gateway # 3");
			designer.drawFlow("Exclusive Gateway # 3", "End Event # 2");
			designer.drawFlow("Exclusive Gateway # 3", "Action # 3");
			designer.drawFlow("Exclusive Gateway # 3", "Task # 1");
			designer.drawFlow("Exclusive Gateway # 3", "Task # 3");
			designer.drawFlow("Action # 3", "Task # 2");
			designer.drawFlow("Task # 1", "Exclusive Gateway # 4");
			designer.drawFlow("Task # 2", "Exclusive Gateway # 4");
			designer.drawFlow("Task # 3", "Exclusive Gateway # 4");		
			designer.drawFlow("Exclusive Gateway # 4", "End Event # 3");		
			designer.saveProcess();
			designer.deleteAction("Lead Start Event # 1");
			designer.deleteAction("Action # 1");
			designer.deleteAction("Exclusive Gateway # 1");
			designer.deleteAction("Message Event # 2");
			designer.deleteAction("Wait Event # 1");
			designer.deleteAction("Message Event # 1");		
			designer.deleteAction("Action # 2");
			designer.deleteAction("Exclusive Gateway # 2");
			designer.deleteAction("End Event # 1");
			designer.deleteAction("Exclusive Gateway # 3");
			designer.deleteAction("End Event # 2");
			designer.deleteAction("Action # 3");
			designer.deleteAction("Task # 1");
			designer.deleteAction("Task # 2");
			designer.deleteAction("Task # 3");
			designer.deleteAction("Exclusive Gateway # 4");
			designer.deleteAction("End Event # 3");
			TestDesigner.testCasePassed(testCaseDrawing);			
		}catch (Exception e) {
			TestDesigner.testCaseFailed(testCaseDrawing);			
		};		
		
		httpClient.getConnectionManager().shutdown();		
		designer.saveProcess();
		//designer.closeDesigner();
		designer.closeSugar();

	}
	
}