import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.apache.http.client.HttpClient;  
import org.apache.http.client.entity.UrlEncodedFormEntity;  
import org.apache.http.client.methods.HttpPost;  
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import java.util.List;
import org.apache.http.entity.StringEntity;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;
import javax.naming.AuthenticationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.openqa.selenium.ie.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;


public class TestDesigner{
	public static TestDesigner designer = new TestDesigner();
	public static WebDriver driver = null;
	
	// This method login to SugarCRM
	public void loginSugar(String user, String password, String instance)throws IOException{
		setConfig();
		switch (getConfig("browser"))
		{
			case "FireFox":
				//driver = new  FirefoxDriver();
				ProfilesIni profile = new ProfilesIni();
				FirefoxProfile ffprofile = profile.getProfile("SELENIUM");
				driver = new FirefoxDriver(ffprofile);
				break;
			case "Internet Explorer":
				System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
				driver = new  InternetExplorerDriver();
				break;
			default:
				System.setProperty("webdriver.chrome.driver", "chromedriver.exe");			
				driver = new ChromeDriver();
				break;
		}
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement sugarInstance = null;
		driver.manage().window().maximize();
		driver.get(instance);
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("username")));
		myDynamicElement.sendKeys(user);
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.name("login_button")).click();
		WebElement myDynamicElement2 = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Done")));		
		myDynamicElement2.click();
	}
	
	// This method open a process
	public void openProcess(String processName)throws Exception{
		driver.findElement(By.linkText(processName)).click();
		Thread.sleep(5000);		
		driver.findElement(By.cssSelector("#content > div > div > div.main-pane.span8 > div > div:nth-child(1) > div > div.headerpane > h1 > div > span.actions.btn-group.detail > a")).click();	
		WebElement myDynamicElement = (new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.name("open_designer")));		
		myDynamicElement.click();		
	}	
	
	// This create a action
	public void createAction(String actionName, int posX, int posY)throws Exception{
		
		String action_id = "";
		switch (actionName)
		{
			case "Start Event Opportunity":
				action_id = "AdamEventOpportunity";
				break;
			case "Start Event Lead":
				action_id = "AdamEventLead";
				break;
			case "Start Event Document":
				action_id = "AdamEventDocument"	;
				break;
			case "Other Module Event":
				action_id = "AdamEventOtherModule";
				break;
			case "Wait":
				action_id = "AdamEventTimer";
				break;
			case "Receive Message":
				action_id = "AdamEventReceiveMessage";
				break;
			case "Send Message":
				action_id = "AdamEventMessage";
				break;
			case "User task":
				action_id = "AdamUserTask";
				break;
			case "Action":
				action_id = "AdamScriptTask";
				break;				
			case "Exclusive":
				action_id = "AdamGatewayExclusive";
				break;
			case "Parallel":
				action_id = "AdamGatewayParallel";
				break;
			case "End":
				action_id = "AdamEventEnd";
				break;
			case "Comment":
				action_id = "AdamTextAnnotation";
				break;
			default:
				action_id = "AdamTextAnnotation";
				break;
		}	

		(new Actions(driver)).dragAndDropBy(driver.findElement(By.id(action_id)), posX, posY).build().perform(); 
		System.out.println ("Creating the action " + actionName);	
	}
	
	// This method change the action type
	public void changeActionType(String actionName, String actionType)throws Exception{
		Actions action = new Actions(driver);
		WebElement actionCanvas = driver.findElement(By.xpath("//span[contains(., \"" + actionName + "\")]"));
		action.contextClick(actionCanvas).perform();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Action Type")));
		driver.findElement(By.linkText("Action Type")).click();
		driver.findElement(By.linkText("Unassigned")).click();
		driver.findElement(By.linkText(actionType)).click();
		System.out.println ("Change action " + actionName + " to type " + actionType);
	}

	// This method convert gateways
	public void convertGateway(String gatewayName, String gatewayType){
		Actions action = new Actions(driver);
		WebElement actionCanvas = driver.findElement(By.xpath("//span[contains(., \"" + gatewayName + "\")]"));
		action.contextClick(actionCanvas).perform();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Convert...")));		
		driver.findElement(By.linkText("Convert...")).click();
		driver.findElement(By.linkText("Exclusive Gateway")).click();
		driver.findElement(By.linkText(gatewayType)).click();
		System.out.println ("Change gateway " + gatewayName + " to type " + gatewayType);
	}

	// This method change result for End events
	public void endEventsResult(String endName, String result)throws Exception{
		Actions action = new Actions(driver);
		WebElement actionCanvas = driver.findElement(By.xpath("//span[contains(., \"" + endName + "\")]"));
		action.contextClick(actionCanvas).perform();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Result")));		
		driver.findElement(By.linkText("Result")).click();
		driver.findElement(By.linkText("Do Nothing")).click();
		driver.findElement(By.linkText(result)).click();
		System.out.println ("Change end " + endName + " to result " + result);
	}	
	
	// This method open settings for Actions
	public void openSettings(String actionName)throws Exception{
		Actions action = new Actions(driver);
		WebElement actionCanvas = driver.findElement(By.xpath("//span[contains(., \"" + actionName + "\")]"));
		action.contextClick(actionCanvas).perform();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Settings...")));		
		driver.findElement(By.linkText("Settings...")).click();
	}

	// This method delete actions
	public void deleteAction(String actionName)throws Exception{
		Actions action = new Actions(driver);
		WebElement actionCanvas = driver.findElement(By.xpath("//span[contains(., \"" + actionName + "\")]"));
		action.contextClick(actionCanvas).perform();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Delete")));		
		driver.findElement(By.linkText("Delete")).click();
		System.out.println ("Deleting action " + actionName);		
	}

	// This method save process
	public void saveProcess() throws Exception{
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("ButtonSave")));		
		driver.findElement(By.id("ButtonSave")).click();
		Thread.sleep(5000);
		System.out.println ("Saving process...");		
	}

	// This method close designer
	public void closeDesigner(){
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#adam_toolbar > div.pmse-toolbar-title > div:nth-child(1) > button")));		
		driver.findElement(By.cssSelector("#adam_toolbar > button")).click();
		System.out.println ("Closing the designer...");		
	}

	// This method set settings for start events
	public void setStartEvent(String actionName, String applyTo, String criteriaField, String criteriaValue)throws Exception{
		designer.openSettings(actionName);
		WebDriverWait wait0 = new WebDriverWait(driver, 50);		
		WebElement element = wait0.until(ExpectedConditions.elementToBeClickable(By.id("evn_params")));			
		element = driver.findElement(By.id("evn_params"));
		Select droplist1 = new Select(element);
		droplist1.selectByVisibleText(applyTo);	
		driver.findElement(By.id("evn_criteria")).click();			
		WebDriverWait wait = new WebDriverWait(driver, 50);		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(., \"Module Field Evaluation\")]")));		
		driver.findElement(By.xpath("//span[contains(., \"Module Field Evaluation\")]")).click();
		WebDriverWait wait2 = new WebDriverWait(driver, 50);		
		element = wait2.until(ExpectedConditions.elementToBeClickable(By.name("expField")));				
		element = driver.findElement(By.name("expField"));		
		Select droplist2 = new Select(element);
		droplist2.selectByVisibleText(criteriaField);	
		driver.findElement(By.name("expValue")).sendKeys(criteriaValue);
		driver.findElement(By.xpath("//input[@type='submit']")).click();	
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}	

	// This method set timer events
	public void setTimerEvent(String actionName, String duration, String unit)throws Exception{
		designer.openSettings(actionName);
		WebDriverWait wait2 = new WebDriverWait(driver, 50);		
		WebElement  element = wait2.until(ExpectedConditions.elementToBeClickable(By.id("evn_duration_criteria")));		
		driver.findElement(By.id("evn_duration_criteria")).sendKeys(duration);		
		element = driver.findElement(By.id("evn_duration_params"));
		Select droplist1 = new Select(element);
		droplist1.selectByVisibleText(unit);	
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}	

	// This method set the receive message
	public void setReceiveMessage(String actionName, String criteriaField, String criteriaValue)throws Exception{
		designer.openSettings(actionName);	
		WebDriverWait wait0 = new WebDriverWait(driver, 50);		
		WebElement element = wait0.until(ExpectedConditions.elementToBeClickable(By.id("evn_criteria")));				
		driver.findElement(By.id("evn_criteria")).click();			
		WebDriverWait wait = new WebDriverWait(driver, 50);		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(., \"Module Field Evaluation\")]")));		
		driver.findElement(By.xpath("//span[contains(., \"Module Field Evaluation\")]")).click();
		WebDriverWait wait2 = new WebDriverWait(driver, 50);		
		element = wait2.until(ExpectedConditions.elementToBeClickable(By.name("expField")));		
		Select droplist2 = new Select(element);
		droplist2.selectByVisibleText(criteriaField);	
		driver.findElement(By.name("expValue")).sendKeys(criteriaValue);
		driver.findElement(By.xpath("//input[@type='submit']")).click();	
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}		
	
	// This method set the send message
	public void setSendMessage(String actionName, String emailTemplate, String emailTo)throws Exception{
		designer.openSettings(actionName);
		WebDriverWait wait0 = new WebDriverWait(driver, 50);		
		WebElement element = wait0.until(ExpectedConditions.elementToBeClickable(By.id("evn_criteria")));		
		element = driver.findElement(By.id("evn_criteria"));
		Select droplist1 = new Select(element);
		droplist1.selectByVisibleText(emailTemplate);
		driver.findElement(By.cssSelector("#address_to > input")).sendKeys(emailTo);
		driver.findElement(By.cssSelector("#address_to > input")).sendKeys(Keys.RETURN);		
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}	

	// This method set the user task
	public void setUserTask(String actionName, String duration, String unit)throws Exception{
		Actions action = new Actions(driver);
		WebElement actionCanvas = driver.findElement(By.xpath("//span[contains(., \"" + actionName + "\")]"));
		action.contextClick(actionCanvas).perform();
		WebDriverWait wait = new WebDriverWait(driver, 50);		
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Forms...")));		
		driver.findElement(By.linkText("Forms...")).click();
		WebDriverWait wait0 = new WebDriverWait(driver, 50);		
		element = wait0.until(ExpectedConditions.elementToBeClickable(By.id("act_reassign")));			
		driver.findElement(By.id("act_reassign")).click();
		driver.findElement(By.id("act_adhoc")).click();	
		driver.findElement(By.linkText("General")).click();			
		driver.findElement(By.linkText("Readonly fields")).click();	
		driver.findElement(By.name("account_name")).click();
		driver.findElement(By.name("last_name")).click();	
		driver.findElement(By.linkText("Required fields")).click();	
		driver.findElement(By.name("title")).click();
		driver.findElement(By.name("website")).click();
		driver.findElement(By.linkText("Expected Time")).click();	
		driver.findElement(By.id("evn_criteria")).sendKeys(duration);
		element = driver.findElement(By.id("evn_params"));		
		Select droplist2 = new Select(element);
		droplist2.selectByVisibleText(unit);	
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}	

	// This method set the business rule
	public void setBusinessRule(String actionName, String businessRule)throws Exception{
		designer.openSettings(actionName);
		WebDriverWait wait0 = new WebDriverWait(driver, 50);		
		WebElement element = wait0.until(ExpectedConditions.elementToBeClickable(By.id("act_fields")));		
		element = driver.findElement(By.id("act_fields"));
		Select droplist1 = new Select(element);
		droplist1.selectByVisibleText(businessRule);
		driver.findElement(By.linkText("Save")).click();		
		System.out.println ("Setting action " + actionName + " ...");		
	}		

	// This method set the assign user
	public void setAssignUser(String actionName, String user)throws Exception{
		designer.openSettings(actionName);
		WebDriverWait wait0 = new WebDriverWait(driver, 50);		
		WebElement element = wait0.until(ExpectedConditions.elementToBeClickable(By.id("act_assign_user")));		
		element = driver.findElement(By.id("act_assign_user"));
		Select droplist1 = new Select(element);
		droplist1.selectByVisibleText(user);
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}	
	
	// This method set the round robin
	public void setRoundRobin(String actionName, String team)throws Exception{
		designer.openSettings(actionName);
		WebDriverWait wait0 = new WebDriverWait(driver, 50);		
		WebElement element = wait0.until(ExpectedConditions.elementToBeClickable(By.id("act_assign_team")));		
		element = driver.findElement(By.id("act_assign_team"));
		Select droplist1 = new Select(element);
		droplist1.selectByVisibleText(team);
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}	

	// This method set the Add Related Record
	public void setAddRelatedRecord(String actionName)throws Exception{
		designer.openSettings(actionName);	
		WebDriverWait wait = new WebDriverWait(driver, 50);		
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("act_field_module")));			
		element = driver.findElement(By.id("act_field_module"));
		Select droplist2 = new Select(element);
		droplist2.selectByVisibleText("Notes (lead_notes - one-to-many)");	
		WebDriverWait wait1 = new WebDriverWait(driver, 50);		
		element = wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[6]/div[2]/div/div[2]/div[2]/div/div[4]/div[2]/a/span")));			
		driver.findElement(By.xpath("/html/body/div[6]/div[2]/div/div[2]/div[2]/div/div[4]/div[2]/input")).sendKeys("Test qa");
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}	

	// This method set the Change field
	public void setChangeField(String actionName)throws Exception{
		designer.openSettings(actionName);		
		WebDriverWait wait0 = new WebDriverWait(driver, 50);		
		WebElement element = wait0.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[6]/div[2]/div/div[2]/div[2]/div/div[5]/div[1]/input")));		
		driver.findElement(By.xpath("/html/body/div[6]/div[2]/div/div[2]/div[2]/div/div[5]/div[1]/input")).click();
		driver.findElement(By.xpath("/html/body/div[6]/div[2]/div/div[2]/div[2]/div/div[5]/div[2]/input")).sendKeys("PMSE");
		driver.findElement(By.xpath("/html/body/div[6]/div[2]/div/div[2]/div[2]/div/div[1]/div[1]/input")).click();
		driver.findElement(By.xpath("/html/body/div[6]/div[2]/div/div[2]/div[2]/div/div[1]/div[2]/input")).sendKeys("Sales");		
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}		

	// This method set the gateways
	public void setGateways(String actionName) throws Exception{
		designer.openSettings(actionName);	
		WebDriverWait wait0 = new WebDriverWait(driver, 50);		
		WebElement element = wait0.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[6]/div[2]/div/div[2]/div/ul/input")));			
		driver.findElement(By.xpath("/html/body/div[6]/div[2]/div/div[2]/div/ul/input")).click();
		WebDriverWait wait = new WebDriverWait(driver, 50);		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(., \"Module Field Evaluation\")]")));		
		driver.findElement(By.xpath("//span[contains(., \"Module Field Evaluation\")]")).click();	
		Thread.sleep(2000);		
		element = driver.findElement(By.name("expField"));		
		Select droplist2 = new Select(element);
		droplist2.selectByVisibleText("Department");
		driver.findElement(By.name("expValue")).sendKeys("QA");			
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.findElement(By.linkText("Save")).click();	
		System.out.println ("Setting action " + actionName + " ...");		
	}

	// This method close the browser
	public void closeSugar(){
		driver.close();	
		driver.quit();	
	}

	// This method draw the flow between two elements.
	public void drawFlow(String actionName, String actionName2)
        {
			int moveValx = 58;
			int moveValy = -16;	
			int moveVal2x = 40;
			int moveVal2y = -15;				
			Actions action = new Actions(driver);
			WebElement actionCanvas = driver.findElement(By.xpath("//span[contains(., \"" + actionName + "\")]"));
			WebElement actionCanvas2 = driver.findElement(By.xpath("//span[contains(., \"" + actionName2 + "\")]"));		
			if (actionName.startsWith("Action"))
			{
				moveValx = 63;
			}
			if (actionName.startsWith("Exclusive"))
			{
				moveValx = 81;
			}
			if (actionName.startsWith("Task"))
			{
				moveValx = 94;
				moveValy = 1;
			}
			if (actionName2.startsWith("Task"))
			{
				moveVal2x = -1;
				moveVal2y = 10;				
			}			
			action.moveToElement(actionCanvas, moveValx, moveValy).clickAndHold().moveToElement(actionCanvas2,moveVal2x,moveVal2y).release().build().perform();
			System.out.println ("Joining " + actionName + " with " + actionName2 + " ...");
        }	

	static void updateJira(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json")
				.accept("application/json").put(ClientResponse.class, data);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
	        else if(response.getStatus() == 403)
                throw new ClientHandlerException("Error " + response.getStatus());
            else if(response.getStatus() != 204) 
                throw new ClientHandlerException("Error " + response.getStatus());		
	}	

	static void testCasePassed(String testCaseId) throws AuthenticationException, ClientHandlerException{
		updateJira(jiraAuth, jiraUrl + testCaseId, issueStatusPassed);
		System.out.println ("Test case "+ testCaseId + " has been PASSED");		
	}	

	static void testCaseFailed(String testCaseId)throws AuthenticationException, ClientHandlerException{
		updateJira(jiraAuth, jiraUrl + testCaseId, issueStatusFailed);
		updateJira(jiraAuth, jiraUrl + testCaseId, issueComment);
		System.out.println ("Test case "+ testCaseId + " has been FAILED!!!!");		
	}	

	static String getConfig(String propertyName)throws IOException{
		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream("default.conf");
 		prop.load(input);
		String result = prop.getProperty(propertyName);
		return result;
	}
	
	static void setConfig()throws IOException{
		jiraUrl = getConfig("jira.url");
		jiraAuth = new String(Base64.encode(getConfig("jira.user") + ":" + getConfig("jira.password")));
		issueStatusFailed = "{\"fields\": {\"" + getConfig("jira.custom") + "\":\"Failed\"}}";
		issueStatusPassed = "{\"fields\": {\"" + getConfig("jira.custom") + "\":\"Passed\"}}";		
		issueComment = "{\"update\": {\"comment\":[{\"add\": {\"body\": \"The user is not able to use this element\"}}]}}";	
	}
	
	static String jiraUrl = "";
	static String jiraAuth = "";
	static String issueStatusFailed = "";
	static String issueStatusPassed = "";		
	static String issueComment = "";	
	
}